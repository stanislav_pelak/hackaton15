_ = require 'lodash'
request = require 'request'
apiKey = "xLYcDom0Iemrr8xlReYJcLUYAbQeq2mLOgzM0dRf"

searchIn = [
  "openfda.generic_name"
  "openfda.brand_name"
  "active_ingredient"
  "openfda.manufacturer_name"
  "openfda.substance_name"
]

module.exports =
  getProducts: (term, searchLimit, callback) ->
    query = (searchIn.join ":#{term}+OR+") + ":#{term}"
    start = (new Date()).getTime()
    url = "https://api.fda.gov/drug/label.json?api_key=#{apiKey}&limit=#{searchLimit}&search=#{query}"
    request
      url: url
    , (err, res) ->
      if err then console.error "Retrieving products from FDA failed miserably:", err
      if res.statusCode isnt 200 then console.error "Retrieving products from FDA failed with status code", res.statusCode
      console.log "#{searchLimit} items retrieved in #{((new Date()).getTime() - start)/1000} s"
      # callback (JSON.parse res.body).results or []
      res = (JSON.parse res.body).results or []
      callback _.map res, (item) ->
        return name: item?.openfda?.brand_name?[0]
