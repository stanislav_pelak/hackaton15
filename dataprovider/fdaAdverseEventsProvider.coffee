request = require 'request'
apiKey = "xLYcDom0Iemrr8xlReYJcLUYAbQeq2mLOgzM0dRf"

searchIn = [
  "patient.drug.medicinalproduct"
]

module.exports =
  getEvents: (drugName, searchLimit, callback) ->
    query = (searchIn.join ":#{drugName}+OR+") + ":#{drugName}"
    start = (new Date()).getTime()
    request
      url: "https://api.fda.gov/drug/event.json?api_key=#{apiKey}&limit=#{searchLimit}&search=#{query}"
    , (err, res) ->
      console.log "#{searchLimit} items retrieved in #{((new Date()).getTime() - start)/1000} s"
      if err then console.error "Retrieving adverse events from FDA failed miserably:", err
      if res.statusCode isnt 200 then console.error "Retrieving adverse events from FDA failed with status code", res.statusCode
      callback JSON.parse res.body
