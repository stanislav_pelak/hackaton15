_ = require 'lodash'
fs = require 'fs'
path = require 'path'

# init
events = {}

# Read dictionaries and parse them to events object
dirsPath = path.join __dirname, "../app_data/ae_dictionaries"
dicts = fs.readdirSync dirsPath
for dict in dicts
  try
    # Language
    lang = /^[^-]*-([a-z]+)\.asc$/.exec(dict)[1]

    # Read terms from the file
    content = (fs.readFileSync path.join dirsPath, dict).toString()
    content = _.compact content.split "\n"
    content = _.map content, (item) ->
      parts = item.split "$"
      return id: parts[0], name: parts[1]
    content = _.uniq(content, 'id');
    events[lang] = content
  catch
    continue

# Export
module.exports =
  getAdverseEvents: (lang) ->
    events[lang] or events.en
