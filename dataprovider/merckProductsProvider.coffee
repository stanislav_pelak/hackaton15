request = require 'request'

products = null

module.exports =
  getProducts: (callback) ->
    if not products
      request
        method: "GET"
        url: "https://iapitest-test.merck.com/merckproducts/v1/products"
        headers:
          "x-MERCK-APIkey": "zkdpYSAK0B3fqL3qLstwi5J7aSEuwHB0"
      , (err, res) ->
        if err then console.error "Retrieving merck products failed miserably:", err
        if res.statusCode isnt 200 then console.error "Retrieving merck products failed with status code", res.statusCode
        products = (JSON.parse res.body).records
        callback products
    else
      callback products
