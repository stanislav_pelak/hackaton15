(function() {

  var model = new (function() {
    this.language = ko.observable(null);

    this.selectLanguage = function(model, e) {
      var lang = $(e.target).attr("data-value");
      $.cookie('i18n', lang);
      window.location.reload();
    };

    submit = function(model) {
      var data = ko.toJS(model.data);
      $.post("/api/submit", data, function(req, res) {

      });
    };

    this.data = {
      repname: ko.observable(),
      repaddress: ko.observable(),
      repemail: ko.observable(),
      repphone: ko.observable(),
      repcontact: ko.observable(),
      patid1: ko.observable(),
      patid2: ko.observable(),
      patgender: ko.observable(),
      patbirth: ko.observable(),
      patage: ko.observable(),
      patweight: ko.observable(),
      patheight: ko.observable(),
      msdmed: {
        name: ko.observable(),
        indication: ko.observable()
      },
      othermed: {
        name: ko.observable(),
        indication: ko.observable()
      },
      advevents: ko.observableArray(),
      adveventsdetails: ko.observable(),
      adveventsonset: ko.observable(),
      pathospitalized: ko.observable()
    };
  })();

  window.AERS = {
    init: function() {
      // Init wizard
      $("#wizard").bwizard({cookie: {name: "wizz"}}).show();
      $('.reportedProduct').selectize({
        valueField: 'resourceName',
        labelField: 'resourceName',
        searchField: 'resourceName',
        options: [],
        create: function(input) {
          return {
            resourceName: input
          }
        },
        render: {
          option: function(item, escape) {
            return '<div data-value="' + '' + '">' + escape(item.resourceName) + '</div>';
          }
        },
        load: function(term, callback) {
          if (!term.length) return callback();
          $.ajax({
            url: '/api/merckproducts',
            type: 'GET',
            data: {term: term},
            error: function() {
              callback();
            },
            success: function(res) {
              callback(res);
            }
          });
        }
      });

      $('.eventSelectMultiple').selectize({
        valueField: 'id',
        labelField: 'name',
        searchField: 'name',
        options: [],
        create: function(input) {
          return {
            evtId: input,
            evtent: input
          }
        },
        render: {
          option: function(item, escape) {
            return '<div>' + escape(item.name) + '</div>';
          }
        },
        load: function(term, callback) {
          if (!term.length) return callback();
          $.ajax({
            url: '/api/adverseevents',
            type: 'GET',
            data: {term: term},
            error: function() {
              callback();
            },
            success: function(res) {
              callback(res);
            }
          });
        }
      });

      $('.otherProduct').selectize({
        valueField: 'name',
        labelField: 'name',
        searchField: 'name',
        options: [],
        create: function(input) {
          return {
            resourceName: input
          }
        },
        render: {
          option: function(item, escape) {
            return '<div>' + escape(item.name) + '</div>';
          }
        },
        load: function(term, callback) {
          if (!term.length) return callback();
          $.ajax({
            url: '/api/otherproducts',
            type: 'GET',
            data: {term: term},
            error: function() {
              callback();
            },
            success: function(res) {
              callback(res);
            }
          });
        }
      });

      // Set localization
      var lang = $.cookie('i18n');
      if(!lang) {
        lang = 'en';
        $.cookie('i18n', 'en');
      }
      model.language(lang);

      // Apply bindings
      ko.applyBindings(model, $("body")[0]);
    }
  };

})();
