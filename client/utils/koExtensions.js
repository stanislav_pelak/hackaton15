(function(){
  ko.bindingHandlers.dateTimePicker = {
    init: function(element, valueAccessor, allBindings, viewModel, bindingContext) {
      var value = ko.unwrap(allBindings().value);
      var opts = valueAccessor();
      opts.format = opts.format || 'Y-m-d H:i';
      opts.dayOfWeekStart = opts.dayOfWeekStart || 1;
      opts.scrollInput = opts.scrollInput || false;
      opts.value = value && Date.parse(value);
      $(element).datetimepicker(opts);

      // Esc key listener
      $(document).unbind("keyup.dropzone");
      $(document).bind("keyup.dropzone", function(e) {
        if (e.keyCode == 27) {
          $("input[data-bind*='dateTimePicker']").datetimepicker("hide");
        }
      });

      ko.utils.domNodeDisposal.addDisposeCallback(element, function() {
        $(element).datetimepicker("destroy");
      });
    },
    update: function(element, valueAccessor, allBindings, viewModel, bindingContext) {
      ko.unwrap(allBindings().value);
    }
  };
})();
