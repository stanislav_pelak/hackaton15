module.exports = function(grunt) {

    grunt.initConfig({

        // (Re)Start the server
        express: {
          dev: {
            options: {
              script: './index.coffee',
              opts: ['./node_modules/.bin/coffee']
            }
          }
        },

        watch: {

          // Reload server when SERVER sources have changed
          coffee: {
            files: [
              "**/*.coffee",
              "**/*.json",
              "!test/**",
              "!node_modules/**"
            ],
            tasks: ["express"],
            options: {
              spawn: false // this doesn't work with mocha tests - watch task for tests below
            }
          },

          // Run unit tests when a coffee file is updated (separate from "coffee" watch task due to some problem with spawn option)
          // tests: {
          //   files: [
          //     "**/*.coffee",
          //     "!node_modules/**"
          //   ],
          //   tasks: ["mochaTest:server"],
          //   options: {
          //     interrupt: true
          //   }
          // },

          // Concatenate, minify and run tests
          admin_js: {
            files: [
              "client/**/*.js"
            ],
            tasks: ["uglify", "concat" /*, "mochaTest:client"*/],
            options: {
              interrupt: true
            }
          },

          // Minify CSSs
          admin_css: {
            files: [
              "client/**/*.css"
            ],
            tasks: ["cssmin"],
            options: {
              interrupt: true
            }
          }

        },

        // Run tests
        // mochaTest: {
        //   server: {
        //     options: {
        //       reporter: "spec",
        //       require: 'coffee-script/register',
        //       interrupt: true
        //     },
        //     src: [
        //       "test/**/*.coffee",
        //       "!test/client/**"
        //     ]
        //   },
        //   client: {
        //     options: {
        //       reporter: "spec",
        //       require: 'coffee-script/register',
        //       interrupt: true
        //     },
        //     src: [
        //       "test/client/**/*.coffee"
        //     ]
        //   }
        // },

        // Uglify and concatenate JS files
        uglify: {
          js: {
            files: {
              'assets/js/script.min.js': [
                'client/**/*.js'
              ]
            }
          }
        },
        concat: {
          js: {
            src: ['client/**/*.js'],
            dest: 'assets/js/script.js'
          }
        },

        // Minify and concatenate CSS files
        cssmin: {
          css: {
            files: {
              'assets/css/styles.min.css': [
                'client/css/**/*.css'
              ]
            }
          }
        },

    });

    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-coffee');
    grunt.loadNpmTasks('grunt-express-server');
    // grunt.loadNpmTasks('grunt-mocha-test');

    // grunt.registerTask("build", [ "concat", "uglify", "cssmin", "mochaTest" ]);
    grunt.registerTask("default", [ "concat", "uglify", "cssmin", "express", "watch" ]);

};
