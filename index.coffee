path = require 'path'
i18n = require 'i18n'
express = require 'express'
# session = require 'express-session'
bodyParser = require 'body-parser'
cookieParser = require 'cookie-parser'
app_config = require './config/config'

app = express()

################################
# Configure the app
################################

app.use cookieParser 'somesecret'
app.set 'view engine', 'ejs'
app.enable 'trust proxy'

# app.use session
#   cookie:
#     maxAge: 1800000 # 30 mins
#   secret: 'somesecret'
#   resave: false
#   saveUninitialized: true


################################
# Auth
################################
# auth = (require './business/auth')(app)

################################
# STATIC route
################################

app.use require './routes/static'

app.use bodyParser.urlencoded extended: true
app.use bodyParser.json()

# Renew cookie before each of the following requests
# app.use (req, res, next) ->
#   # Break session hash / force express to spit out a new cookie
#   req.session._garbage = Date();
#   req.session.touch();
#   next();


################################
# I18n
################################

i18n.configure
  locales: ['en', 'fr']
  defaultLocale: 'en'
  cookie: 'i18n'
  directory: './app_data/i18n'
  updateFiles: false
  indent: "  "
  objectNotation: false

app.use i18n.init


################################
# API Routes
################################

app.use '/api', require './routes/api'

################################
# VIEWS
################################

app.use '/', (require './routes/main')(null)

################################
# START
################################
app.listen app_config.PORT, -> console.log "listening on port #{app_config.PORT}"
