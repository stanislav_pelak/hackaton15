_ = require 'lodash'
express = require 'express'
Router = express.Router()

merckProducts = require "../dataprovider/merckProductsProvider"
fdaProducts = require "../dataprovider/fdaProductsProvider"
adverseEvents = require "../dataprovider/adverseEventsProvider"

# fdaAdverseEvents = require "../dataprovider/fdaAdverseEventsProvider"

# console.log adverseEvents.getAdverseEvents "fr"
# fdaAdverseEvents.getEvents "silicea", 10, (events) -> console.log events

# merckProducts.getProducts()

products = null
getProducts = (cb) ->
  if not products then return merckProducts.getProducts cb
  cb products

getEvents = (lang, cb) ->
  cb adverseEvents.getAdverseEvents lang

Router.get '/merckproducts', (req, res, next) ->
  term = req.query?.term
  if not term then res.sendStatus 400
  terms = _.compact term.toLowerCase().split " "

  getProducts (products) ->
    filtered = _.filter products, (prod) ->
      name = prod.resourceName.toLowerCase()
      matches = true
      _.forEach terms, (t) ->
        matches = matches && name.indexOf(t) > -1
      return matches

    res.send filtered

Router.get '/adverseevents', (req, res, next) ->
  term = req.query?.term
  if not term then res.sendStatus 400
  terms = _.compact term.toLowerCase().split " "

  getEvents req.getLocale(), (events) ->
    filtered = _.filter events, (evt) ->
      name = evt.name.toLowerCase()
      matches = true
      _.forEach terms, (t) ->
        matches = matches && name.indexOf(t) > -1
      return matches

    res.send filtered

Router.get '/otherproducts', (req, res, next) ->
  term = req.query?.term
  if not term then res.sendStatus 400

  fdaProducts.getProducts term, 10, (products) ->
    res.send products

Router.post '/submit', (req, res, next) ->
  console.log req.body
  res.sendStatus 204

module.exports = Router
