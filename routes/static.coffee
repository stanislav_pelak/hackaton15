path = require 'path'
express = require 'express'
Router = express.Router()

module.exports = Router.use '/assets', express.static path.join __dirname, '..', 'assets'
