express = require 'express'
Router = express.Router()

adverseEvents = require "../dataprovider/adverseEventsProvider"
merckProducts = require "../dataprovider/merckProductsProvider"
fdaProducts = require "../dataprovider/fdaProductsProvider"
fdaAdverseEvents = require "../dataprovider/fdaAdverseEventsProvider"

# console.log adverseEvents.getAdverseEvents "fr"
# merckProducts.getProducts (products) -> console.log products
# fdaProducts.getProducts "silicea", 10, (products) -> console.log products
# fdaAdverseEvents.getEvents "silicea", 10, (events) -> console.log events

module.exports = (app_data) ->

  Router.get '/', (req, res, next) ->
    res.render 'index'
  Router
